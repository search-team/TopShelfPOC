﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace TopShelfPOC
{
    public class DemoController : ApiController
    {
        // GET api/demo 
        public IEnumerable<string> Get()
        {
            var logger = LogManager.GetLogger("DemoController");
            logger.Info("GET api/demo");
            return new string[] { "Hello", "World" };
        }

        // GET api/demo/5 
        public string Get(int id)
        {
            var logger = LogManager.GetLogger("DemoController");
            logger.Info("GET api/demo");
            return "Hello, World!";
        }

        // POST api/demo 
        public void Post([FromBody]string value)
        {
            var logger = LogManager.GetLogger("DemoController");
            logger.Info("POST api/demo");
        }

        // PUT api/demo/5 
        public void Put(int id, [FromBody]string value)
        {
            var logger = LogManager.GetLogger("DemoController");
            logger.Info("PUT api/demo/5");
        }

        // DELETE api/demo/5 
        public void Delete(int id)
        {
            var logger = LogManager.GetLogger("DemoController");
            logger.Info("DELETE api/demo/5");
        }
    }
}
