﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace TopShelfPOC
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.UseLog4Net("Logging.config");
                x.Service<SelfHostedService>(s =>
                {
                    s.ConstructUsing(name => new SelfHostedService());                    
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                   
                });                
                x.RunAsLocalSystem();    
                x.SetDescription("Self hosted web API Demo with Topshelf");
                x.SetDisplayName("Self Host Web API Demo");
                x.SetServiceName("SelfHostWebAPI");
            });
        }
    }
}
